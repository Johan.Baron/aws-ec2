#!/bin/bash
sudo yum install -y docker
sudo systemctl start docker
sudo su ec2-user
mkdir -p /home/ec2-user/myapache
cd /home/ec2-user/myapache

## create index.html
cat << 'EOF' > index.html
<html>
<h1>Sagen EC2-ASG-LB Test</h1>
<p>This is index.html being served from inside a Docker container!</p>
</html>
EOF

## create health
cat << 'EOF' > health
<html>
<h1>Sagen EC2-ASG-LB Test</h1>
<p>This is health, being served from inside a Docker container!</p>
</html>
EOF

sudo docker run -d --name my-apache-app -p 8080:80 -v $(pwd):/usr/local/apache2/htdocs/ httpd:2.4
curl localhost:8080/health
