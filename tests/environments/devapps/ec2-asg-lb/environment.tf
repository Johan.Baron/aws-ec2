module "test_module_ec2-asg-alb" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-asg-lb"

  app_env       = var.app_env
  os_type       = "al2023"
  prefix        = var.prefix
  vpc_id        = "vpc-0dcae5fa1996af114"
  ec2_subnets   = ["subnet-07fb063eecacaae16", "subnet-09eca3a8035921ba2"]
  lb_subnets    = ["subnet-07fb063eecacaae16", "subnet-09eca3a8035921ba2"]
  lb_type       = "alb"
  cert_arn      = "arn:aws:acm:ca-central-1:708815303669:certificate/6826cfb5-ae21-4d72-a7ff-5b4b324ef782"
  instance_type = "t3.medium"
  sg_ingress    = []
  sg_egress = []
  lb_sg_ingress = [
    {
      description = "Allow ALB Inbound from All on 443"
      ip_protocol = "tcp"
      from_port   = "443"
      to_port     = "443"
      cidr_ipv4   = "0.0.0.0/0"
    }
  ]
  lb_sg_to_tg_port  = 8080
  ec2_sg_to_lb_port = 8080
  tg_protocol       = "HTTP"
  ebs_backup        = "False"

  custom_user_data = "./user_data.sh"
}
