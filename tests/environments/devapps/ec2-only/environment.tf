module "test_module_ec2-only-any-ip" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-only"

  app_env       = var.app_env
  os_type       = "al2"
  ami_id = "ami-0f682fb0d11c2ae31"
  prefix        = var.prefix
  vpc_id        = "vpc-0dcae5fa1996af114"
  ec2_subnet_id = "subnet-092597f57febf8156"
  instance_type = "t3.medium"

  sg_ingress = [{
    description = "Test sg ingress 80"
    protocol    = "tcp"
    from_port   = "80"
    to_port     = "80"
    cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Test sg ingress 443"
      protocol    = "tcp"
      from_port   = "443"
      to_port     = "443"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  ebs_backup = "False"
}

module "test_module_ec2-only-private-ip" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-only"

  app_env       = var.app_env
  private_ip_address = "10.70.199.75"
  os_type       = "al2"
  ami_id = "ami-0f682fb0d11c2ae31"
  prefix        = "${var.prefix}-privateIP"
  vpc_id        = "vpc-0dcae5fa1996af114"
  ec2_subnet_id = "subnet-092597f57febf8156"
  instance_type = "t3.medium"
  ebs_backup = "False"
}