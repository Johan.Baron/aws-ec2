terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  # backend "s3" {
  #   bucket         = "sagen-aws-devapps-tfstate-ca-central-1"
  #   key            = "Admin/gitlab/peo/iac-terraform/aws-ec2/test.tfstate" #name of the S3 object that will store the state file
  #   region         = "ca-central-1"
  #   dynamodb_table = "sagen-aws-devapps-tfstate-ca-central-1"
  # }
}

provider "aws" {
  region = "ca-central-1"
  default_tags {
    tags = {
      Environment = "Dev"
      Account     = "devapps"
      Team        = "Platform Engineering and Ops"
      Managed_By  = "gitlab/peo/iac-terraform/aws-ec2/terraform/test"
      Owner       = "IT.DevOps@sagen.ca"
      Project     = "DevOps"
      Version     = "June 2024"
      Department  = "IT"
    }
  }
}

variable "prefix" {
  type    = string
  default = "peo-test-ec2-only"
}

variable "app_env" {
  type    = string
  default = "dev"
}