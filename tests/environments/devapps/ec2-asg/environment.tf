module "test_module_ec2-asg" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-asg"

  app_env       = var.app_env
  os_type       = "rhel8"
  prefix        = var.prefix
  vpc_id        = "vpc-0dcae5fa1996af114"
  ec2_subnets   = ["subnet-07fb063eecacaae16", "subnet-09eca3a8035921ba2"]
  instance_type = "t3.medium"
  ami_id        = "ami-067deadd431782242" #AL2023
  sg_ingress = [{
    description = "Test sg ingress 80"
    protocol    = "tcp"
    from_port   = "80"
    to_port     = "80"
    cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Test sg ingress 443"
      protocol    = "tcp"
      from_port   = "443"
      to_port     = "443"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  sg_egress = []
  custom_user_data = "./user_data.sh"
  # custom_tags = [{
  #   key="JIRA"
  #   value="SIT-1234test"
  # }]
  ebs_backup = "False"
}

# module "test_module_ec2-asg-win" {
#   source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-asg"

#   app_env       = var.app_env
#   os_type       = "win"
#   prefix        = var.prefix
#   vpc_id        = "vpc-0dcae5fa1996af114"
#   ec2_subnets   = ["subnet-07fb063eecacaae16", "subnet-09eca3a8035921ba2"]
#   instance_type = "t3.medium"
#   key_name = "cyberark-connector-test-key"
#   disk_size = "30"
#   ami_id = "ami-068cf6c073ccf2420" # Microsoft Windows Server 2022 Base
#   sg_ingress = [{
#     description = "Test sg ingress 80"
#     protocol    = "tcp"
#     from_port   = "80"
#     to_port     = "80"
#     cidr_blocks = ["0.0.0.0/0"]
#     },
#     {
#       description = "Test sg ingress 443"
#       protocol    = "tcp"
#       from_port   = "443"
#       to_port     = "443"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   ]
#   sg_egress = [
#     {
#       description = "Test sg egress 443"
#       protocol    = "tcp"
#       from_port   = "443"
#       to_port     = "443"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   ]
# }
