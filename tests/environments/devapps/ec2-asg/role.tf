resource "aws_iam_policy" "test_ec2" {
  name        = "${var.prefix}-ec2-policy-${var.app_env}"
  description = "IAM role policy for ${var.prefix}-ec2-role-${var.app_env}"
  policy      = file("./policy.json")
  tags = {
    Name = "${var.prefix}-ec2-policy-${var.app_env}"
  }
}

resource "aws_iam_role_policy_attachment" "test_ec2" {
  role       = module.test_module_ec2-asg.ec2_role_name
  policy_arn = aws_iam_policy.test_ec2.arn
}
