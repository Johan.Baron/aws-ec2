# AWS-EC2

This repo contains the Terraform modules which follow Sagen's IAC standards and best practices. The modules should be used in other Terraform-based Gitlab repos for creating EC2 related infrastructure. 

## Modules
1. ec2-asg
This module will create an AutoScalingGroup (ASG) with the specified number of (min/desired/max) EC2s.

2. ec2-asg-lb
This module will create a Load Balancer (currently only ALB is implemented and tested) with an AutoScalingGroup (ASG) with the specified number of (min/desired/max) EC2s.

3. ec2-only
This module will create a launch template and optional EC2 based on the launch template. User can also specify a fixed private IP for the EC2 in the launch template. 

## How to use
To use the modules, for example:

```
module "test_module_ec2-asg" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-asg"

  app_env       = var.app_env
  os_type       = "rhel8"
  prefix        = var.prefix
  vpc_id        = "vpc-0dcae5fa1996af114"
  ec2_subnets   = ["subnet-07fb063eecacaae16","subnet-09eca3a8035921ba2"]
  instance_type = "t3.medium"
  ami_id = "ami-01ba2527c256d3670" # Verified provider RHEL-8.7.0_HVM-20230330-x86_64-56-Hourly2-GP2
  sg_ingress = [{
    description = "Test sg ingress 80"
    protocol    = "tcp"
    from_port   = "80"
    to_port     = "80"
    cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Test sg ingress 443"
      protocol    = "tcp"
      from_port   = "443"
      to_port     = "443"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  sg_egress = [
    {
      description = "Test sg egress 443"
      protocol    = "tcp"
      from_port   = "443"
      to_port     = "443"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

```
For a list of availble inputs, see individual module's README. 

For a full example of how to use the module, see folder `./tests/environments/devapps/`.

## How to contribute
- Clone the repo
- Create a test branch, e.g. `git checkout -b feature-X`
- Add your test case inside folder `tests`
- Create the TF file with reference to your branch, e.g.:
```
module "test_module_ec2_asg_feature_X" {
  source = "git::https://gitlab.com/ca-sagen/peo/iac-terraform/aws-ec2.git//modules/ec2-asg?ref=feature-X"
...
}
```
- Update module's README.md using [terraform-docs](https://github.com/terraform-docs/terraform-docs), for example:
```
cd modules
terraform-docs markdown table --output-file README.md --output-mode inject ec2-asg
```

## References
1. [EC2 instance types (CPU, RAM, Storage)](https://aws.amazon.com/ec2/instance-types/)
2. [EC2 CPU options](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/cpu-options-supported-instances-values.html)
3. [EC2 Pricing](https://aws.amazon.com/ec2/pricing/on-demand/)