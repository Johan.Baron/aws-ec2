output "ec2_role_arn" {
  value       = aws_iam_role.ec2.arn
  description = "EC2 role ARN."
}

output "ec2_role_name" {
  value       = aws_iam_role.ec2.name
  description = "EC2 role name."
}

output "asg_name" {
  value       = aws_autoscaling_group.ec2.name
  description = "EC2 ASG name."
}

output "ec2_sg_name" {
  value       = var.os_type == "win" ? aws_security_group.ec2_win[0].name : aws_security_group.ec2_unix[0].name
  description = "EC2 SG name."
}

output "ec2_sg_id" {
  value       = var.os_type == "win" ? aws_security_group.ec2_win[0].id : aws_security_group.ec2_unix[0].id
  description = "ID of the SG on the EC2"
}

output "lb_name" {
  value       = aws_lb.ec2.name
  description = "Load balancer name."
}

output "lb_sg_name" {
  value       = aws_security_group.lb.name
  description = "Load balancer SG name."
}

output "lb_sg_id" {
  value       = aws_security_group.lb.id
  description = "ID of the SG on the load balancer"
}

output "lb_tg_name" {
  value       = aws_lb_target_group.ec2.name
  description = "Target group name."
}