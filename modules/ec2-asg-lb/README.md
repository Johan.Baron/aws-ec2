<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_group.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) | resource |
| [aws_iam_instance_profile.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.aws_ssm_managedinstancecore](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_launch_template.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) | resource |
| [aws_lb.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) | resource |
| [aws_lb_listener.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener) | resource |
| [aws_lb_target_group.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_security_group.ec2_unix](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.ec2_win](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.lb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_vpc_security_group_egress_rule.lb_default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_security_group_egress_rule) | resource |
| [aws_vpc_security_group_ingress_rule.lb_custom](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_security_group_ingress_rule) | resource |
| [aws_ami.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_default_tags.base](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/default_tags) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | AMI id value. | `string` | n/a | yes |
| <a name="input_app_env"></a> [app\_env](#input\_app\_env) | App environment. Must be one of sbx, dev, qat, stg, trn or prd. | `string` | n/a | yes |
| <a name="input_asg_desired_capacity"></a> [asg\_desired\_capacity](#input\_asg\_desired\_capacity) | Desired ASG capacity. | `number` | `1` | no |
| <a name="input_asg_max_size"></a> [asg\_max\_size](#input\_asg\_max\_size) | Maximum ASG capacity. | `number` | `1` | no |
| <a name="input_asg_min_size"></a> [asg\_min\_size](#input\_asg\_min\_size) | Minimum ASG capacity. | `number` | `1` | no |
| <a name="input_cert_arn"></a> [cert\_arn](#input\_cert\_arn) | n/a | `string` | n/a | yes |
| <a name="input_cookie_duration"></a> [cookie\_duration](#input\_cookie\_duration) | n/a | `number` | `86400` | no |
| <a name="input_core_count"></a> [core\_count](#input\_core\_count) | Choose a valid CPU core count for the selected instance\_type. | `number` | `1` | no |
| <a name="input_custom_tags"></a> [custom\_tags](#input\_custom\_tags) | Custom tags to be applied to EC2 instance and volume | <pre>list(object({<br>    key   = string<br>    value = string<br>  }))</pre> | `[]` | no |
| <a name="input_custom_user_data"></a> [custom\_user\_data](#input\_custom\_user\_data) | Path to the custom user data script file. | `string` | `""` | no |
| <a name="input_disk_iops"></a> [disk\_iops](#input\_disk\_iops) | EC2 disk IOPs. | `number` | `null` | no |
| <a name="input_disk_size"></a> [disk\_size](#input\_disk\_size) | EC2 root device volume size. | `string` | `"20"` | no |
| <a name="input_disk_type"></a> [disk\_type](#input\_disk\_type) | EC2 root device volume type. | `string` | `"gp3"` | no |
| <a name="input_ebs_backup"></a> [ebs\_backup](#input\_ebs\_backup) | Backup tag value for EBS volume auto-backup. | `string` | `"True"` | no |
| <a name="input_ec2_sg_to_lb_port"></a> [ec2\_sg\_to\_lb\_port](#input\_ec2\_sg\_to\_lb\_port) | Port number for EC2 SG from LB SG inbound | `number` | `443` | no |
| <a name="input_ec2_subnets"></a> [ec2\_subnets](#input\_ec2\_subnets) | List of subnets for the ASG. | `list(string)` | n/a | yes |
| <a name="input_health_check_code"></a> [health\_check\_code](#input\_health\_check\_code) | The health check code | `string` | `"200"` | no |
| <a name="input_health_check_path"></a> [health\_check\_path](#input\_health\_check\_path) | The path for the health check including / in the front | `string` | `"/health"` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Instance type | `string` | `"t3.medium"` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Cusom SSH Key name. Assume it exists already. | `string` | `""` | no |
| <a name="input_lb_sg_ingress"></a> [lb\_sg\_ingress](#input\_lb\_sg\_ingress) | Custom LB SG ingress rules. | `any` | n/a | yes |
| <a name="input_lb_sg_to_tg_port"></a> [lb\_sg\_to\_tg\_port](#input\_lb\_sg\_to\_tg\_port) | Port number for LB outbound to Target's SG | `number` | `443` | no |
| <a name="input_lb_subnets"></a> [lb\_subnets](#input\_lb\_subnets) | List of subnets for the Load Balancer. | `list(string)` | n/a | yes |
| <a name="input_lb_type"></a> [lb\_type](#input\_lb\_type) | Type of Load Balancer. | `string` | n/a | yes |
| <a name="input_os_type"></a> [os\_type](#input\_os\_type) | OS type of the base AMI. | `string` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Project name and component if any. | `string` | n/a | yes |
| <a name="input_sg_egress"></a> [sg\_egress](#input\_sg\_egress) | Custom EC2 SG egress rules. | `any` | `[]` | no |
| <a name="input_sg_ingress"></a> [sg\_ingress](#input\_sg\_ingress) | Custom EC2 SG ingress rules. | `any` | `[]` | no |
| <a name="input_ssl_policy"></a> [ssl\_policy](#input\_ssl\_policy) | n/a | `string` | `"ELBSecurityPolicy-TLS13-1-2-2021-06"` | no |
| <a name="input_stickiness_enabled"></a> [stickiness\_enabled](#input\_stickiness\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_tg_protocol"></a> [tg\_protocol](#input\_tg\_protocol) | The protocol on the Target Group | `string` | `"HTTPS"` | no |
| <a name="input_threads_per_core"></a> [threads\_per\_core](#input\_threads\_per\_core) | Choose a valid threads per core count for the selected instance\_type. | `number` | `2` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC id value. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_asg_name"></a> [asg\_name](#output\_asg\_name) | EC2 ASG name. |
| <a name="output_ec2_role_arn"></a> [ec2\_role\_arn](#output\_ec2\_role\_arn) | EC2 role ARN. |
| <a name="output_ec2_role_name"></a> [ec2\_role\_name](#output\_ec2\_role\_name) | EC2 role name. |
| <a name="output_ec2_sg_id"></a> [ec2\_sg\_id](#output\_ec2\_sg\_id) | ID of the SG on the EC2 |
| <a name="output_ec2_sg_name"></a> [ec2\_sg\_name](#output\_ec2\_sg\_name) | EC2 SG name. |
| <a name="output_lb_name"></a> [lb\_name](#output\_lb\_name) | Load balancer name. |
| <a name="output_lb_sg_id"></a> [lb\_sg\_id](#output\_lb\_sg\_id) | ID of the SG on the load balancer |
| <a name="output_lb_sg_name"></a> [lb\_sg\_name](#output\_lb\_sg\_name) | Load balancer SG name. |
| <a name="output_lb_tg_name"></a> [lb\_tg\_name](#output\_lb\_tg\_name) | Target group name. |
<!-- END_TF_DOCS -->