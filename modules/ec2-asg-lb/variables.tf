variable "prefix" {
  type        = string
  description = "Project name and component if any."
}

variable "app_env" {
  type = string
  validation {
    condition     = contains(["sbx", "dev", "qat", "stg", "trn", "prd"], var.app_env)
    error_message = "Argument 'app_env' must be one of sbx, dev, qat, stg, trn or prd."
  }
  description = "App environment. Must be one of sbx, dev, qat, stg, trn or prd."
}

variable "ami_id" {
  type        = string
  description = "AMI id value."
}

variable "vpc_id" {
  type        = string
  description = "VPC id value."
}

variable "ec2_subnets" {
  type        = list(string)
  description = "List of subnets for the ASG."
}

variable "lb_subnets" {
  type        = list(string)
  description = "List of subnets for the Load Balancer."
}

variable "lb_type" {
  type        = string
  description = "Type of Load Balancer."
  validation {
    condition     = contains(["alb", "gwlb", "nlb"], var.lb_type)
    error_message = "Argument 'lb_type' must be one of 'alb', 'gwlb' or 'nlb'."
  }
}

variable "sg_ingress" {
  type        = any
  description = "Custom EC2 SG ingress rules."
  default = []
}

variable "sg_egress" {
  type        = any
  description = "Custom EC2 SG egress rules."
  default = []
}

variable "lb_sg_ingress" {
  type        = any
  description = "Custom LB SG ingress rules."
}

variable "lb_sg_to_tg_port" {
  type        = number
  description = "Port number for LB outbound to Target's SG"
  default     = 443
}

variable "ec2_sg_to_lb_port" {
  type        = number
  description = "Port number for EC2 SG from LB SG inbound"
  default     = 443
}

variable "ssl_policy" {
  type    = string
  default = "ELBSecurityPolicy-TLS13-1-2-2021-06"
}

variable "cert_arn" {
  type = string
}

variable "tg_protocol" {
  type        = string
  default     = "HTTPS"
  description = "The protocol on the Target Group"
}

variable "health_check_path" {
  type        = string
  default     = "/health"
  description = "The path for the health check including / in the front"
}

variable "health_check_code" {
  type        = string
  default     = "200"
  description = "The health check code"
}

variable "stickiness_enabled" {
  type    = bool
  default = false
}

variable "cookie_duration" {
  type    = number
  default = 86400
}


variable "os_type" {
  type        = string
  description = "OS type of the base AMI."
  validation {
    condition     = contains(["win", "rhel8", "rhel9", "al2", "al2023"], var.os_type)
    error_message = "Argument 'os_type' must be one of 'win', 'rhel8', 'rhel9', 'al2', 'al2023'."
  }
}

variable "asg_desired_capacity" {
  type        = number
  default     = 1
  description = "Desired ASG capacity."
}

variable "asg_max_size" {
  type        = number
  default     = 1
  description = "Maximum ASG capacity."
}
variable "asg_min_size" {
  type        = number
  default     = 1
  description = "Minimum ASG capacity."
}

variable "instance_type" {
  type        = string
  default     = "t3.medium"
  description = "Instance type"
}

variable "key_name" {
  type        = string
  default     = ""
  description = "Cusom SSH Key name. Assume it exists already."
}

variable "disk_type" {
  type        = string
  default     = "gp3"
  description = "EC2 root device volume type."
}

variable "disk_size" {
  type        = string
  default     = "20" # GB 
  description = "EC2 root device volume size."
}

variable "ebs_backup" {
  type        = string
  default     = "True"
  description = "Backup tag value for EBS volume auto-backup."
}

variable "disk_iops" {
  type        = number
  default     = null
  description = "EC2 disk IOPs."
}

variable "core_count" {
  type        = number
  default     = 1 # default CPU core for t3.medium
  description = "Choose a valid CPU core count for the selected instance_type."
}

variable "threads_per_core" {
  type        = number
  default     = 2 # default threads per core for t3.medium
  description = "Choose a valid threads per core count for the selected instance_type."
}

variable "custom_user_data" {
  type        = string
  default     = ""
  description = "Path to the custom user data script file."
}

variable "custom_tags" {
  type = list(object({
    key   = string
    value = string
  }))
  description = "Custom tags to be applied to EC2 instance and volume"
  default     = []
}