locals {
  lb_type = {
    "alb" : "application",
    "nlb" : "network",
    "gwlb" : "gateway"
  }
}

resource "aws_security_group" "lb" {
  name        = "${local.lb_prefix}-sg-${var.app_env}"
  description = "Security Group for for ${local.lb_prefix} load balancer"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${local.lb_prefix}-sg-${var.app_env}"
  }
}

resource "aws_vpc_security_group_egress_rule" "lb_default" {
  security_group_id            = aws_security_group.lb.id
  description                  = "Default outbound to Target Group resource SG"
  from_port                    = var.lb_sg_to_tg_port
  to_port                      = var.lb_sg_to_tg_port
  ip_protocol                  = "tcp"
  referenced_security_group_id = var.os_type == "win" ? aws_security_group.ec2_win[0].id : aws_security_group.ec2_unix[0].id
}

resource "aws_vpc_security_group_ingress_rule" "lb_custom" {
  for_each = {
    for ind, rule in var.lb_sg_ingress :
    rule.cidr_ipv4 => rule
  }
  security_group_id = aws_security_group.lb.id
  description       = each.value.description
  from_port         = try(each.value.from_port, null)
  to_port           = try(each.value.to_port, null)
  ip_protocol       = each.value.ip_protocol
  cidr_ipv4         = each.value.cidr_ipv4
}


resource "aws_lb" "ec2" {
  name               = "${local.lb_prefix}-${var.app_env}"
  internal           = true
  load_balancer_type = lookup(local.lb_type, var.lb_type)
  security_groups    = [aws_security_group.lb.id]
  subnets            = var.lb_subnets
  tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
    {
      Name = "${local.lb_prefix}-${var.app_env}"
    }
  )
}

resource "aws_lb_target_group" "ec2" {
  name     = "${local.lb_prefix}-tg-${var.app_env}"
  port     = var.lb_sg_to_tg_port
  protocol = var.tg_protocol
  vpc_id   = var.vpc_id

  health_check {
    path     = var.health_check_path
    matcher  = var.health_check_code
    port     = var.lb_sg_to_tg_port
    protocol = var.tg_protocol
  }
  stickiness {
    cookie_duration = var.cookie_duration
    enabled         = var.stickiness_enabled
    type            = "lb_cookie"
  }
  tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
    {
      Name = "${local.lb_prefix}-tg-${var.app_env}"
    }
  )
}

resource "aws_lb_listener" "ec2" {
  load_balancer_arn = aws_lb.ec2.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.cert_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ec2.arn
  }
  tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
    {
      Name = "${local.lb_prefix}-listener-${var.app_env}"
    }
  )
}
