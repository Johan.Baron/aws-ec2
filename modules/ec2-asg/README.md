<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.35.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_group.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) | resource |
| [aws_iam_instance_profile.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.aws_ssm_managedinstancecore](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_launch_template.ec2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) | resource |
| [aws_security_group.ec2_unix](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.ec2_win](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ami.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_default_tags.base](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/default_tags) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | AMI id value. | `string` | n/a | yes |
| <a name="input_app_env"></a> [app\_env](#input\_app\_env) | App environment. Must be one of sbx, dev, qat, stg, trn or prd. | `string` | n/a | yes |
| <a name="input_asg_desired_capacity"></a> [asg\_desired\_capacity](#input\_asg\_desired\_capacity) | Desired ASG capacity. | `number` | `1` | no |
| <a name="input_asg_max_size"></a> [asg\_max\_size](#input\_asg\_max\_size) | Maximum ASG capacity. | `number` | `1` | no |
| <a name="input_asg_min_size"></a> [asg\_min\_size](#input\_asg\_min\_size) | Minimum ASG capacity. | `number` | `1` | no |
| <a name="input_core_count"></a> [core\_count](#input\_core\_count) | Choose a valid CPU core count for the selected instance\_type. | `number` | `1` | no |
| <a name="input_custom_tags"></a> [custom\_tags](#input\_custom\_tags) | Custom tags to be applied to EC2 instance and volume | <pre>list(object({<br>    key   = string<br>    value = string<br>  }))</pre> | `[]` | no |
| <a name="input_custom_user_data"></a> [custom\_user\_data](#input\_custom\_user\_data) | Path to the custom user data script file. | `string` | `""` | no |
| <a name="input_disk_iops"></a> [disk\_iops](#input\_disk\_iops) | EC2 disk IOPs. | `number` | `null` | no |
| <a name="input_disk_size"></a> [disk\_size](#input\_disk\_size) | EC2 root device volume size. | `string` | `"20"` | no |
| <a name="input_disk_type"></a> [disk\_type](#input\_disk\_type) | EC2 root device volume type. | `string` | `"gp3"` | no |
| <a name="input_ebs_backup"></a> [ebs\_backup](#input\_ebs\_backup) | Backup tag value for EBS volume auto-backup. | `string` | `"True"` | no |
| <a name="input_ec2_subnets"></a> [ec2\_subnets](#input\_ec2\_subnets) | List of subnets for the ASG. | `list(string)` | n/a | yes |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Instance type | `string` | `"t3.medium"` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Cusom SSH Key name. Assume it exists already. | `string` | `""` | no |
| <a name="input_os_type"></a> [os\_type](#input\_os\_type) | OS type of the base AMI. | `string` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Project name and component if any. | `string` | n/a | yes |
| <a name="input_sg_egress"></a> [sg\_egress](#input\_sg\_egress) | Custom SG egress rules. | `any` | `[]` | no |
| <a name="input_sg_ingress"></a> [sg\_ingress](#input\_sg\_ingress) | Custom SG ingress rules. | `any` | `[]` | no |
| <a name="input_threads_per_core"></a> [threads\_per\_core](#input\_threads\_per\_core) | Choose a valid threads per core count for the selected instance\_type. | `number` | `2` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC id value. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_asg_name"></a> [asg\_name](#output\_asg\_name) | EC2 ASG name. |
| <a name="output_ec2_role_arn"></a> [ec2\_role\_arn](#output\_ec2\_role\_arn) | EC2 role ARN. |
| <a name="output_ec2_role_name"></a> [ec2\_role\_name](#output\_ec2\_role\_name) | EC2 role name. |
| <a name="output_ec2_sg_id"></a> [ec2\_sg\_id](#output\_ec2\_sg\_id) | ID of the SG on the EC2 |
| <a name="output_ec2_sg_name"></a> [ec2\_sg\_name](#output\_ec2\_sg\_name) | EC2 SG name. |
<!-- END_TF_DOCS -->