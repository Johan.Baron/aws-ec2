resource "aws_iam_role" "ec2" {
  name = "${var.prefix}-ec2-role-${var.app_env}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })

  tags = {
    Name = "${var.prefix}-ec2-role-${var.app_env}"
  }
}

resource "aws_iam_instance_profile" "ec2" {
  name = "${var.prefix}-ec2-profile-${var.app_env}"
  role = aws_iam_role.ec2.name
}

resource "aws_iam_role_policy_attachment" "aws_ssm_managedinstancecore" {
  role       = aws_iam_role.ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}