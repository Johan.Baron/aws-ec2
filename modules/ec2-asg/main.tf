data "aws_default_tags" "base" {}
data "aws_vpc" "selected" {
  id = var.vpc_id
}
data "aws_ami" "selected" {
  filter {
    name   = "image-id"
    values = [var.ami_id]
  }
}

locals {
  ec2_name             = "${var.prefix}-ec2-${var.app_env}"
  launch_template_name = "${var.prefix}-ec2-lt-${var.app_env}"
  asg_name             = "${var.prefix}-ec2-asg-${var.app_env}"
  default_user_data    = var.os_type == "win" ? ("${path.module}/userdata/setup_win.ps1") : ("${path.module}/userdata/setup_${var.os_type}.sh")
  tags_map = length(var.custom_tags) == 0 ? {} : {
    for item in var.custom_tags : item.key => item.value
  }
}

resource "aws_autoscaling_group" "ec2" {
  name                = local.asg_name
  desired_capacity    = var.asg_desired_capacity
  max_size            = var.asg_max_size
  min_size            = var.asg_min_size
  vpc_zone_identifier = var.ec2_subnets
  # target_group_arns   = [aws_lb_target_group.alb.arn]
  launch_template {
    id      = aws_launch_template.ec2.id
    version = "$Latest"
  }
  tag {
    key                 = "ASG"
    value               = local.asg_name
    propagate_at_launch = true
  }
}

# data "template_file" "user_data" {
#   template = var.os_type == "win" ? ("${path.module}/userdata/setup_win.ps1") : ("${path.module}/userdata/setup_unix.sh")
#   vars = {
#     cyberark_bucket_name = var.cyberark_bucket_name
#   }
# }

resource "aws_launch_template" "ec2" {
  name          = local.launch_template_name
  description   = "Launch template for ${var.prefix} ASG EC2s"
  image_id      = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_name
  block_device_mappings {
    device_name = data.aws_ami.selected.root_device_name
    ebs {
      volume_size           = var.disk_size
      delete_on_termination = false
    }

  }
  cpu_options {
    core_count       = var.core_count
    threads_per_core = var.threads_per_core # total vCPU = core_count x threads_per_core
  }
  ebs_optimized = true
  iam_instance_profile {
    arn = aws_iam_instance_profile.ec2.arn
  }

  monitoring {
    enabled = true
  }
  vpc_security_group_ids = [var.os_type == "win" ? aws_security_group.ec2_win[0].id : aws_security_group.ec2_unix[0].id]

  tag_specifications {
    resource_type = "instance"
    tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = local.ec2_name
        Backup = var.ebs_backup
      }
    )

  }

  tag_specifications {
    resource_type = "volume"
    tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = "${local.ec2_name}-volume"
        Backup = var.ebs_backup
      }
    )

  }

  #user_data = base64encode(data.template_file.user_data.rendered)
  user_data = var.custom_user_data != "" ? base64encode(var.custom_user_data) : base64encode(templatefile(local.default_user_data, {}))
  metadata_options {
    http_tokens = "required"
  }

}