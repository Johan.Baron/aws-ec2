resource "aws_security_group" "ec2_unix" {
  count       = var.os_type == "win" ? 0 : 1
  name        = "${var.prefix}-ec2-sg-${var.app_env}"
  description = "Security Group for ${var.prefix} EC2"
  vpc_id      = var.vpc_id
  dynamic "ingress" {
    for_each = var.sg_ingress
    content {
      description     = ingress.value.description
      from_port       = ingress.value.from_port
      to_port         = ingress.value.to_port
      protocol        = ingress.value.protocol
      cidr_blocks     = try(ingress.value.cidr_blocks, null)
      self            = try(ingress.value.self, null)
      security_groups = try(ingress.value.security_groups, null)
    }
  }
  dynamic "egress" {
    for_each = var.sg_egress
    content {
      description     = egress.value.description
      from_port       = egress.value.from_port
      to_port         = egress.value.to_port
      protocol        = egress.value.protocol
      cidr_blocks     = try(egress.value.cidr_blocks, null)
      self            = try(egress.value.self, null)
      security_groups = try(egress.value.security_groups, null)
    }
  }
  ingress {
    description = "For CArk PSM subnets"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.64.192.32/28", "10.64.192.48/28", "10.64.192.16/28"]
  }
  egress {
    description = "For Alert Logic IDS"
    from_port   = 7777
    to_port     = 7777
    protocol    = "tcp"
    cidr_blocks = ["10.64.192.11/32"]
  }
  egress {
    description = "Allow all traffic outbound on 443" # necessary for AL agent update, log collection
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.prefix}-ec2-sg-${var.app_env}"
  }
}
