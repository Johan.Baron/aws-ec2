#!/bin/bash
sudo su -
yum update -y
yum install zip -y

# Download and install AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install -i /usr/local/aws-cli -b /usr/local/bin
/usr/local/bin/aws --version

# install ssm agent to allow SSM
sudo yum install -y https://s3.ca-central-1.amazonaws.com/amazon-ssm-ca-central-1/latest/linux_amd64/amazon-ssm-agent.rpm
systemctl stop amazon-ssm-agent
systemctl enable amazon-ssm-agent
systemctl start amazon-ssm-agent
