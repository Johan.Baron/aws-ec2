variable "prefix" {
  type        = string
  description = "Project name and component if any."
}

variable "app_env" {
  type = string
  validation {
    condition     = contains(["sbx", "dev", "qat", "stg", "trn", "prd"], var.app_env)
    error_message = "Argument 'app_env' must be one of sbx, dev, qat, stg, trn or prd."
  }
  description = "App environment. Must be one of sbx, dev, qat, stg, trn or prd."
}

variable "ami_id" {
  type        = string
  description = "AMI id value."
}

variable "vpc_id" {
  type        = string
  description = "VPC id value."
}

variable "ec2_subnets" {
  type        = list(string)
  description = "List of subnets for the ASG."
}

variable "sg_ingress" {
  # type = list(object({
  #   description = string
  #   protocol    = string
  #   from_port   = string
  #   to_port     = string
  #   cidr_blocks = list(string)
  # }))
  type        = any
  description = "Custom SG ingress rules."
  default = []
}

variable "sg_egress" {
  # type = list(object({
  #   description = string
  #   protocol    = string
  #   from_port   = string
  #   to_port     = string
  #   cidr_blocks = list(string)
  # }))
  type        = any
  description = "Custom SG egress rules."
  default = []
}

variable "os_type" {
  type        = string
  description = "OS type of the base AMI."
  validation {
    condition     = contains(["win", "rhel8", "rhel9", "al2", "al2023"], var.os_type)
    error_message = "Argument 'os_type' must be one of 'win', 'rhel8', 'rhel9', 'al2', 'al2023'."
  }
}

variable "asg_desired_capacity" {
  type        = number
  default     = 1
  description = "Desired ASG capacity."
}

variable "asg_max_size" {
  type        = number
  default     = 1
  description = "Maximum ASG capacity."
}
variable "asg_min_size" {
  type        = number
  default     = 1
  description = "Minimum ASG capacity."
}

variable "instance_type" {
  type        = string
  default     = "t3.medium"
  description = "Instance type"
}

variable "key_name" {
  type        = string
  default     = ""
  description = "Cusom SSH Key name. Assume it exists already."
}

variable "disk_type" {
  type        = string
  default     = "gp3"
  description = "EC2 root device volume type."
}

variable "disk_size" {
  type        = string
  default     = "20" # GB 
  description = "EC2 root device volume size."
}

variable "ebs_backup" {
  type        = string
  default     = "True"
  description = "Backup tag value for EBS volume auto-backup."
}

variable "disk_iops" {
  type        = number
  default     = null
  description = "EC2 disk IOPs."
}

variable "core_count" {
  type        = number
  default     = 1 # default CPU core for t3.medium
  description = "Choose a valid CPU core count for the selected instance_type."
}

variable "threads_per_core" {
  type        = number
  default     = 2 # default threads per core for t3.medium
  description = "Choose a valid threads per core count for the selected instance_type."
}

variable "custom_user_data" {
  type        = string
  default     = ""
  description = "Path to the custom user data script file."
}

variable "custom_tags" {
  type = list(object({
    key   = string
    value = string
  }))
  description = "Custom tags to be applied to EC2 instance and volume"
  default     = []
}