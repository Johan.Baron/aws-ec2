output "ec2_role_arn" {
  value       = aws_iam_role.ec2.arn
  description = "EC2 role ARN."
}

output "ec2_role_name" {
  value       = aws_iam_role.ec2.name
  description = "EC2 role name."
}

output "ec2_sg_name" {
  value       = var.os_type == "win" ? aws_security_group.ec2_win[0].name : aws_security_group.ec2_unix[0].name
  description = "EC2 SG name."
}

output "ec2_sg_id" {
  value       = var.os_type == "win" ? aws_security_group.ec2_win[0].id : aws_security_group.ec2_unix[0].id
  description = "ID of the SG on the EC2"
}

output "lt_name" {
  value       = local.launch_template_name
  description = "Launch template name"
}

output "lt_id" {
  value       = var.private_ip_address == "" ? aws_launch_template.ec2_any_ip[0].id : aws_launch_template.ec2_private_ip[0].id
  description = "Launch template ID"
}

output "lt_latest_version" {
  value       = var.private_ip_address == "" ? aws_launch_template.ec2_any_ip[0].latest_version : aws_launch_template.ec2_private_ip[0].latest_version
  description = "Launch template latest version"
}


output "lt_default_version" {
  value       = var.private_ip_address == "" ? aws_launch_template.ec2_any_ip[0].default_version : aws_launch_template.ec2_private_ip[0].default_version
  description = "Launch template default version"
}

output "ec2_primary_interface_id" {
  value       = var.lt_only == "" ? "" : (var.private_ip_address == "" ? aws_instance.ec2_any_ip[0].primary_network_interface_id : aws_instance.ec2_private_ip[0].primary_network_interface_id)
  description = "EC2 primary network interface"
}

