<powershell>
$awsCliInstallUrl = "https://awscli.amazonaws.com/AWSCLIV2.msi"
try {
    $awsCliStatus = Invoke-WebRequest -Method Head -Uri $awsCliInstallUrl -TimeoutSec 10
    if($awsCliStatus.StatusCode -eq 200) {
        Write-Host "starting aws cli installation" -ForegroundColor DarkGray
        Start-Process "msiexec.exe" -Wait -PassThru -ArgumentList "/i", $awsCliInstallUrl, "/qn", "/norestart"
    }
}
catch {
    $statusCode = $_.Exception.Response.StatusCode.value__
    Write-Error "$awsCliInstallUrl is not reachable. Status code: $statuscode"
}
</powershell>