#!/bin/bash
sudo yum update -y

# Verify AWSCLI, SSM agent
which aws
/usr/bin/aws --version
systemctl status amazon-ssm-agent
