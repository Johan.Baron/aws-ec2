data "aws_default_tags" "base" {}
data "aws_ami" "selected" {
  filter {
    name   = "image-id"
    values = [var.ami_id]
  }
}

locals {
  ec2_name             = "${var.prefix}-ec2-${var.app_env}"
  launch_template_name = "${var.prefix}-ec2-lt-${var.app_env}"
  default_user_data    = var.os_type == "win" ? ("${path.module}/userdata/setup_win.ps1") : ("${path.module}/userdata/setup_${var.os_type}.sh")
  tags_map = length(var.custom_tags) == 0 ? {} : {
    for item in var.custom_tags : item.key => item.value
  }
}

resource "aws_instance" "ec2_any_ip" {
  count = var.lt_only == false && var.private_ip_address == "" ? 1 : 0
  launch_template {
    id      = aws_launch_template.ec2_any_ip[0].id
    version = var.select_lt_version == "" ? "$Latest" : var.select_lt_version
  }
  subnet_id = var.ec2_subnet_id

  tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = local.ec2_name
        Backup = var.ebs_backup
      }
  )
  depends_on = [aws_launch_template.ec2_any_ip]
}

resource "aws_instance" "ec2_private_ip" {
  count = var.lt_only == false && var.private_ip_address != "" ? 1 : 0
  launch_template {
    id      = aws_launch_template.ec2_private_ip[0].id
    version = var.select_lt_version == "" ? "$Latest" : var.select_lt_version
  }
  tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = local.ec2_name
        Backup = var.ebs_backup
      }
  )
  depends_on = [aws_launch_template.ec2_private_ip]
}