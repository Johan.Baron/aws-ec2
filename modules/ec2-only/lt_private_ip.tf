
resource "aws_launch_template" "ec2_private_ip" {
  count         = var.private_ip_address != "" ? 1 : 0
  name          = local.launch_template_name
  description   = "Launch template for ${var.prefix} EC2"
  image_id      = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_name
  block_device_mappings {
    device_name = data.aws_ami.selected.root_device_name
    ebs {
      volume_size           = var.disk_size
      delete_on_termination = false
    }

  }
  cpu_options {
    core_count       = var.core_count
    threads_per_core = var.threads_per_core # total vCPU = core_count x threads_per_core
  }
  ebs_optimized = true
  iam_instance_profile {
    arn = aws_iam_instance_profile.ec2.arn
  }

  monitoring {
    enabled = true
  }

  network_interfaces {
    private_ip_address = var.private_ip_address
    security_groups    = [var.os_type == "win" ? aws_security_group.ec2_win[0].id : aws_security_group.ec2_unix[0].id]
    subnet_id = var.ec2_subnet_id
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = local.ec2_name
        Backup = var.ebs_backup
      }
    )

  }
  tag_specifications {
    resource_type = "volume"
    tags = merge(lookup(data.aws_default_tags.base, "tags"), local.tags_map,
      {
        Name   = "${local.ec2_name}-volume"
        Backup = var.ebs_backup
      }
    )

  }

  user_data = var.custom_user_data != "" ? filebase64(var.custom_user_data) : base64encode(templatefile(local.default_user_data, {}))
  metadata_options {
    http_tokens = "required"
  }

}